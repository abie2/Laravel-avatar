<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Laravolt\Avatar\Facade;

Route::get('/', function () {
    /** Memindahkan gambar ke folder public/avatar/ */
    $path = public_path().'/avatar/';

    $name = "Joko Widodo";

    /** Mengganti name file dan extension */
    $filename = 'avatar_' . str_slug($name, '_') . '_' .date('His') . '.png';

    Avatar::create($name)->save($path . $filename);
});
